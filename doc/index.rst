.. This file is part of trytoncommunity-sort-invoice-grouping.
   Licensed under the GNU Free Documentation License v1.3 or any later version.
   The COPYRIGHT file at the top level of this repository contains the
   full copyright notices and license terms.
   SPDX-License-Identifier: GFDL-1.3-or-later

############################
Sort Invoice Grouping Module
############################

.. If you encounter syntax errors reported by 'twine check':
   Please mind the comment below :-)

.. Please write some words about what this module does
   and for what purpose was it written.
   A single sentence is usually not sufficient.

.. The toctree will automatically be removed when building the distribution
   package,
   see https://www.tryton.org/develop/guidelines/documentation#index.rst

.. toctree::
   :maxdepth: 2

   setup
   usage
   configuration
   design
   reference
   releases

Dieses Modul sortiert Rechnungspositionen nach dem Lieferdatum und ergänzt
die Rechnung mit entsprechenden Kommentaren.

Wenn es für einen Kunden mehrere Verkaufsvorgänge gibt, die in eine
Rechnung geschrieben werden (Option *gruppieren*\ ), so ist nicht sichergestellt,
das die Rechnungspositionen in der Reihenfolge der Lieferungen erstellt werden.

Daher kann mit diesem Modul die Rechnung vor der Festschreibung entsprechend
umsortiert werden. Dabei ist das Lieferdatum der einzelnen Rechnungspositionen
ausschlaggebend. Für jede Gruppe von Rechnungspositionen mit dem selben
Lieferdatum wird ein Kommentar mit Bestell- und Lieferdatum eingefügt.

Die Sortierung erfolgt durch das Setzen des Feldes *Reihenfolge* in der
Rechungsposition. So kann die Sortierreihenfolge auch noch nachträglich
geändert werden.
