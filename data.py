import logging

from trytond import i18n
from trytond.pool import Pool

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class SortInvoiceLine:
    """Invoice line with necessary items for sorting
    """

    def __init__(self, invoiceline, sale_date, shipping_date):
        # instance of invoice.line
        self.invoiceline = invoiceline
        # instances of date
        self.sale_date = sale_date
        self.shipping_date = shipping_date
        # product description
        if self.invoiceline.product is not None:
            self.description = str(self.invoiceline.product.name)
        else:
            self.description = "n.a."
        # sequence number (will be transferred to invoice.line.sequence)
        self.sequence = 0

    def __str__(self):
        return " | ".join([
            "{:>5}".format(self.invoiceline.id),
            "{:>3}".format(self.sequence),
            str(self.sale_date),
            str(self.shipping_date),
            self.description
                ])

    def save(self):
        """Write sequence to the database and save the changes
        """
        self.invoiceline.sequence = self.sequence
        self.invoiceline.save()


class SortInvoiceLineComment:
    """Comment line for order/delivery information
    """

    def __init__(self, invoice, model, description):
        # instance of account.invoice
        self.invoice = invoice
        # model for invoice.line
        self.model = model
        # description and sequence number
        # (will be transferred to invoice.line fields)
        self.description = description
        self.sequence = 0

    def __str__(self):
        return " | ".join([
            "{:>5}".format("new"),
            "{:>3}".format(self.sequence),
            self.description
                ])

    def save(self):
        """Create a new recored in invoice.line and save the description
           and sequence of the comment
        """
        line = self.model()
        line.invoice = self.invoice
        line.type = "comment"
        line.description = self.description
        line.sequence = self.sequence
        line.save()


class SortInvoice:
    """Main data object
    """

    def __init__(self, invoice, date_fmt):
        # instance of account.invoice
        self.invoice = invoice
        # trytond model for invoice.line and sale.line
        pool = Pool()
        self.model_invoice_line = pool.get('account.invoice.line')
        self.model_sale_line = pool.get('sale.line')
        # list of instances of SortInvoiceLine
        self.lines = []
        # list of instances of SortInvoiceLine and
        # SortInvoiceLineComment (final)
        self.lines_with_comments = []
        # format string for dates
        self.date_fmt = date_fmt
        logger.debug("date format: %s", self.date_fmt)

    def __str__(self):
        """First return string repr of self.lines, later when comments
           are added, return string repr of self.lines_with_comments
        """
        string = []
        string.append(
            "  id  | seq | sale_date  | ship_date  | description"
            )
        string.append(
            "------+-----+------------+------------+------------"
            )
        if len(self.lines_with_comments) == 0:
            for line in self.lines:
                string.append(str(line))
        else:
            for line in self.lines_with_comments:
                string.append(str(line))
        return "\n".join(string)

    def append_line(self, line, sale_date, shipping_date):
        """Append instance of SortInvoiceLine
        """
        sortinvoiceline = SortInvoiceLine(line, sale_date, shipping_date)
        self.lines.append(sortinvoiceline)

    def append_comment(self, sale_date, shipping_date):
        """Append a comment in the final list
        """
        description = "{} {}; {} {}".format(
            i18n.gettext(
                'sort_invoice_grouping.msg_sort_invoice_order'
                ),
            sale_date.strftime(self.date_fmt),
            i18n.gettext(
                'sort_invoice_grouping.msg_sort_invoice_delivery'
                ),
            shipping_date.strftime(self.date_fmt)
        )

        comment = SortInvoiceLineComment(self.invoice,
            self.model_invoice_line, description
            )
        self.lines_with_comments.append(comment)

    def parse_lines(self, lines):
        """Parse invoice lines and build instances of SortInvoiceLine
        """
        for line in lines:
            logger.debug("invoice.line: id: %s, type: %s",
                line.id, line.type
                )
            # Check for correct type and correct origin
            if not line.type == "line":
                continue
            origin = str(line.origin)
            if not origin.startswith("sale.line"):
                continue
            # Debug product name
            if line.product is not None:
                logger.debug("invoice.line: product.name: %s",
                    line.product.name
                    )
            # Find sale.line
            table, record = origin.split(",")

            references = self.model_sale_line.search(['id', '=', record])
            if len(references) == 0:
                logger.warning(
                    "parse_lines: id %s not found in sale.line", record
                    )
                continue
            saleline = references[0]

            logger.debug("sale.line: id: %s, shipping_date: %s",
                saleline.id, saleline.shipping_date
                )
            if saleline.shipping_date is None:
                logger.warning(
                    "sale.line: id:%s has no shipping_date", saleline.id
                )
                continue

            # Get sale
            sale = saleline.sale
            logger.debug(
                "sale: id: %s, sale_date: %s, shipping_date: %s",
                sale.id, sale.sale_date, sale.shipping_date
                )
            # Add line
            self.append_line(line, sale.sale_date, saleline.shipping_date)

    def sort_lines(self):
        """Sort instances of SortInvoiceLine
           (sorting creteria is shipping_date)
           Algorithm see
           https://de.wikipedia.org/wiki/Bubblesort#Algorithmus
        """
        n = len(self.lines)
        if n > 1:
            # main loop: equal to "repeat ... until(not swapped)"
            while (True):
                swapped = False
                # inner loop: equal to "for i=0; i<n-1; i++"
                #                   or "for i:= 0 to n-2 do"
                for i in range(n - 1):
                    # ToDo: Perhaps second sorting creteria sale_date
                    if self.lines[i].shipping_date > \
                            self.lines[i + 1].shipping_date:
                        tmp = self.lines[i]
                        self.lines[i] = self.lines[i + 1]
                        self.lines[i + 1] = tmp
                        swapped = True
                n = n - 1
                if not swapped:
                    break

    def add_comments(self):
        """Add comments and build the final list
        """
        n = len(self.lines)
        if n > 0:
            # The lines will be parsed in reverse order and be
            # appended to lines_with_comments. If the shipping date
            # is changing, a comment will be added.
            i = n - 1
            while (True):
                self.lines_with_comments.append(self.lines[i])
                if i == 0:
                    break
                if self.lines[i].shipping_date \
                        != self.lines[i - 1].shipping_date or \
                        self.lines[i].sale_date \
                        != self.lines[i - 1].sale_date:
                    self.append_comment(self.lines[i].sale_date,
                                        self.lines[i].shipping_date
                                        )
                i = i - 1
            # At the end of the list always a comment will be added
            # (which is later the first item of the final list)
            self.append_comment(self.lines[i].sale_date,
                                self.lines[i].shipping_date
                                )
            # Because the list has been built in reverse order, the
            # order has to be reversed now
            self.lines_with_comments.reverse()
            # Set the sequence field with ascending values.
            # A step of 10 is used to make changing the order
            # in the client more easy (by manual typing the
            # value in the sequence field)
            i = 10
            for line in self.lines_with_comments:
                line.sequence = i
                i = i + 10

    def save(self):
        """Save the changes
        """
        for line in self.lines_with_comments:
            line.save()
