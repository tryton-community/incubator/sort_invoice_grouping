import logging

from trytond.model import ModelView
from trytond.pool import Pool, PoolMeta

from .data import SortInvoice

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({'sort_lines': {}})

    @classmethod
    @ModelView.button
    def sort_lines(cls, invoices):
        pool = Pool()
        # get language infos
        Lang = pool.get('ir.lang')
        lang = Lang.get()

        for invoice in invoices:
            logger.info("sorting invoice: id %s: recepient %s",
                invoice.id, invoice.party.name
                )

            sortinvoice = SortInvoice(invoice, lang.date)
            sortinvoice.parse_lines(invoice.lines)

            logger.debug("sortinvoicelines: unsorted\n%s", sortinvoice)
            sortinvoice.sort_lines()
            logger.debug("sortinvoicelines: sorted\n%s", sortinvoice)
            sortinvoice.add_comments()
            logger.info("sortinvoicelines: final\n%s", sortinvoice)
            sortinvoice.save()
            logger.info("changes saved")
