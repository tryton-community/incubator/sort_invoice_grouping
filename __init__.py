# This file is part of trytoncommunity-sort-invoice-grouping.
# Licensed under the GNU General Public License v3 or later (GPLv3+).
# The COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.pool import Pool

from . import invoice

__all__ = ['register']


def register():
    Pool.register(
        invoice.Invoice,
        module='sort_invoice_grouping', type_='model')
#     Pool.register(
#         module='sort_invoice_grouping', type_='wizard')
#     Pool.register(
#         module='sort_invoice_grouping', type_='report')
